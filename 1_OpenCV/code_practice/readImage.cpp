#include <opencv2/opencv.hpp>
#include <iostream>
#include <math.h>

using namespace std;
using namespace cv;

int main(int argc,char **argv) {
	Mat src = imread("F:/opencv/car.jpg");
	if (src.empty()) {
		printf_s("could not open image");
		return -1;
	}

	namedWindow("opencv setup demo",CV_WINDOW_AUTOSIZE);
	imshow("opencv setup demo",src);

	namedWindow("output setup demo", CV_WINDOW_AUTOSIZE);
	Mat outputImage;
	cvtColor(src,outputImage,CV_BGR2GRAY);
	imshow("output setup demo", outputImage);

	imwrite("F:/opencv/carBlack.png", outputImage);

	waitKey(0);

	return 0;
}
