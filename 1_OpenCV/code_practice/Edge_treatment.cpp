#include <opencv2/opencv.hpp>
#include <iostream>

using namespace cv;
using namespace std;

int main(int argc, char **argv) {
	Mat dst, src, gray_src;
	const char* inputTitle  = "input image";
	const char* outputTitle = "output image";

	src = imread("F:/opencv/Sunny.jpg");	
	if (!src.data) {
		cout<<"could not open image";
		return -1;
	}
	namedWindow(inputTitle,CV_WINDOW_AUTOSIZE);
	imshow(inputTitle, src);

	int top    = (int)(0.1*src.rows);
	int bottom = (int)(0.1*src.rows);
	int left = (int)(0.1*src.cols);
	int right = (int)(0.1*src.cols);

	RNG ring(12345);
	int c = 0;
	int borderType = BORDER_DEFAULT;
	for (;;) {
		c = waitKey(500);
		if ((char)c == 27) {
			break;
		}
		else if ((char)c == 'r') {
			borderType = BORDER_REPLICATE;
		}
		else if ((char)c == 'v') {
			borderType = BORDER_WRAP;
		}
		else if ((char)c == 'c') {
			borderType = BORDER_CONSTANT;
		}
		Scalar color = Scalar(ring.uniform(0, 255), ring.uniform(0, 255), ring.uniform(0, 255));
		copyMakeBorder(src,dst,top,bottom,left,right,borderType,color);
		imshow(outputTitle,dst);
	}

	waitKey(0);
	return 0;
}
