cmake_minimum_required(VERSION 2.8)

project(practice01)

set( CMAKE_BUILD_TYPE  "Release")
set(  CMAKE_CXX_FLAGS "-std=c++11 -O3")

list( APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake_moudules)

find_package( Ceres REQUIRED)
include_directories ( ${CERES_INCLUDE_DIRS})

find_package( OpenCV REQUIRED)
include_directories( ${OpencvCV_DIRS} )


add_executable(practice01 main.cpp)

target_link_libraries(practice01 ${CERES_LIBRARIES} ${OpenCV_LIBS})
