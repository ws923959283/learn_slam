#include <iostream>
#include <ceres/ceres.h>
#include <chrono>

using namespace ceres;
using namespace std;

struct CostFunctor{
    template <typename T>
    bool operator()(const T* const x,T* residual) const{
        residual[0] = (10.0)-x[0];
        return true;
    }
    
};

int main(int argc, char **argv) {
    
    
    double inital_x = 5.0;
    double x = inital_x;
    
    Problem problem;
    
    CostFunction* cost_function = new AutoDiffCostFunction<CostFunctor,1,1>(new CostFunctor);
    
    problem.AddResidualBlock(cost_function,nullptr,&x);
    
    Solver::Options options;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;
    Solver::Summary summary;
    Solve(options,&problem,&summary);
    
    std::cout<<summary.BriefReport()<<endl;
    std::cout << "x : " << inital_x << " -> " << x << endl;
    
    std::cout << "Hello, world!" << std::endl;
    
    return 0;
}
